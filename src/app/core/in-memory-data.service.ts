import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Post } from '../post/post';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const posts = [
      { id: 11, name: 'Batman', description: 'LoremIpsum' },
      { id: 12, name: 'Narco', description: 'LoremIpsum' },
      { id: 13, name: 'Bombasto', description: 'LoremIpsum' },
      { id: 14, name: 'Keera', description: 'LoremIpsum' },
      { id: 15, name: 'Wukong', description: 'LoremIpsum' },
      { id: 16, name: 'Superman', description: 'LoremIpsum' },
      { id: 17, name: 'Dynama', description: 'LoremIpsum' },
      { id: 18, name: 'Dr IQ', description: 'LoremIpsum' },
      { id: 19, name: 'Lin', description: 'LoremIpsum' },
      { id: 20, name: 'Storm', description: 'LoremIpsum' },
    ];
    return { posts };
  }

  // Overrides the genId method to ensure that a hero always has an id.
  // If the heroes array is empty,
  // the method below returns the initial number (11).
  // if the heroes array is not empty, the method below returns the highest
  // hero id + 1.
  genId(posts: Post[]): number {
    return posts.length > 0
      ? Math.max(...posts.map((post) => post.id)) + 1
      : 11;
  }
}
