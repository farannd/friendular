import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { Post } from '../../../post/post';
@Injectable({
  providedIn: 'root',
})
export class PostService {
  [x: string]: any;
  private urlTemplate = 'api/posts'; // URL to web api
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  url = "http://localhost:3000/posts";

  constructor(private http: HttpClient) { }

  /** Get All Posts */
  getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(this.url).pipe(
      tap((_) => this.log('fetched Posts')),
      catchError(this.handleError<Post[]>('getPosts', []))
    );
  }

  /** Get post Detail */
  getPost(id: number): Observable<Post> {
    const url = `${this.url}/${id}`;
    return this.http.get<Post>(url).pipe(
      tap((_) => this.log(`fetched post id=${id}`)),
      catchError(this.handleError<Post>(`getPost id=${id}`))
    );
  }

  searchPosts(term: string): Observable<Post[]> {
    if (!term.trim()) {
      return of([]);
    }
    return this.http.get<Post[]>(`${this.url}/?name=${term}`).pipe(
      tap(x => x.length ? this.log(`found posts matching "${term}"`) :
        this.log(`no posts matching "${term}"`)),

      catchError(this.handleError<Post[]>('searchPosts', []))
    );
  }

  /** POST: add a new hero to the server */
  addPost(post: Post): Observable<Post> {
    return this.http.post<Post>(this.url, post, this.httpOptions).pipe(
      tap((newPost: Post) => this.log(`added post w/ id=${newPost.id}`)),
      catchError(this.handleError<Post>('addPost'))
    );
  }

  /** DELETE: delete the post from the server */
  deletePost(id: number): Observable<Post> {
    // const url = `${this.urlTemplate}/${id}`;

    return this.http.delete<Post>(this.url + "/" + id, this.httpOptions).pipe(
      tap((_) => this.log(`deleted post id=${id}`)),
      catchError(this.handleError<Post>('deletePost'))
    );
  }

  // UPDATE: update a hero to the server\
  updatePost(post: Post): Observable<Post> {
    console.log(post)
    return this.http.put<Post>(this.url + "/" + post.id, post, this.httpOptions).pipe(
      tap(_ => this.log(`updated post id=${post.id}`)),

      catchError(this.handleError<any>('updatePost'))
    );
  }

  /** Print message to console */
  private log(message: string) {
    console.log(message);
  }

  /** Handle Error method */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
