import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, map } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  [x: string]: any;
  private urlTemplate = 'api/users';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json ' })
  }

  url = "http://localhost:3000/users"

  constructor(private httpClient: HttpClient) { }

  postUser(data: any) {
    return this.httpClient.post<any>(this.url, data)
      .pipe(map((res: any) => {
        return res;
      }))
  }

  findUser() {
    return this.httpClient.get<any>(this.url)
      .pipe(map((res: any) => {
        return res;
      }))
  }

  private log(message: string) {
    console.log(message);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
