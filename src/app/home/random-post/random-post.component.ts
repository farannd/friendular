import { Component, OnInit } from '@angular/core';

import { PostService } from 'src/app/core/services/post/post.service';
import { Post } from 'src/app/post/post';

@Component({
  selector: 'app-random-post',
  templateUrl: './random-post.component.html',
  styleUrls: ['./random-post.component.css'],
})
export class RandomPostComponent implements OnInit {
  posts: Post[] = [];
  spinner: boolean = true;
  notPosts: boolean = true;

  constructor(private postService: PostService) { }

  ngOnInit(): void {
    this.getPosts();
  }

  refresh(): void {
    this.getPosts();
  }

  getPosts(): void {
    this.spinner = true;
    this.postService.getPosts().subscribe((posts) => {
      let postArr: Post[] = [];
      let iteration = 1;

      if (posts.length >= 4) iteration = 4;

      for (let i = 0; i < iteration; i++) {
        postArr.push(posts[Math.floor(Math.random() * posts.length)]);
      }
      this.posts = postArr;
      this.spinner = false;
      if (this.posts.length !== 0) this.notPosts = false;
      if (this.posts.length === null) this.notPosts = true;
    });
  }
}
