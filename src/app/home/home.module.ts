import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { JumbotronComponent } from './jumbotron/jumbotron.component';
import { RandomPostComponent } from './random-post/random-post.component';

@NgModule({
  declarations: [HomeComponent, JumbotronComponent, RandomPostComponent],
  imports: [CommonModule, HomeRoutingModule, HomeRoutingModule],
})
export class HomeModule {}
