import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/core/services/user/user.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {
  public loginForm!: FormGroup

  constructor(private formBuilder: FormBuilder, private user: UserService, private router: Router, private http: HttpClient) { }

  ngOnInit(): void {
    this.loginForm =  this.formBuilder.group({
      email:[''],
      password:['']
    })
  }

  LoginUser(){
    this.user.findUser().subscribe(
    res => {
      const user = res.find((a:any)=>{
        console.log(a.email);
        return a.email === this.loginForm.value.email && a.password === this.loginForm.value.password
      });
      console.log(user);
      if(user){
        alert("Login Success!!");
        this.loginForm.reset();
        this.router.navigate(['home']);
      }else{
        alert("user not found!");
      }
    }, err =>{
      alert("Something went wrong");
    }
    )
  }
}
