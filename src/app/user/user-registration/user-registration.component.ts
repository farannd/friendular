import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/core/services/user/user.service';
import { UserRegister } from 'src/app/user.Model';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.css']
})
export class UserRegistrationComponent implements OnInit {
  userModelObj: UserRegister = new UserRegister;
  signUpForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private user: UserService, private router: Router) { }

  ngOnInit(): void {
    this.signUpForm = this.formBuilder.group({
      name: [''],
      email: [''],
      password: [''],
      phone: ['']
    })
  }

  createUser() {
    this.userModelObj.name = this.signUpForm.value.name;
    this.userModelObj.email = this.signUpForm.value.email;
    this.userModelObj.password = this.signUpForm.value.password;
    this.userModelObj.phone = this.signUpForm.value.phone;

    this.user.postUser(this.userModelObj).subscribe({
      next: (v) => { console.log(v) },
      error: (e) => {
        alert("Error")
        console.log(e)
      },
      complete: () => {
        console.log('complete')
        alert("Data Saved")
        this.signUpForm.reset();
        this.router.navigate(['user/login']);
      }
    })
  }
}
