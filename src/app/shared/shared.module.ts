import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AppRoutingModule } from '../app-routing.module';
import { HomeRoutingModule } from '../home/home-routing.module';
import { PostRoutingModule } from '../post/post-routing.module';

@NgModule({
  declarations: [HeaderComponent, FooterComponent],
  imports: [CommonModule, HomeRoutingModule, PostRoutingModule],
  exports: [HeaderComponent, FooterComponent],
})
export class SharedModule {}
