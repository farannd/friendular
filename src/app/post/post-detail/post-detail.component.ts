import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { PostService } from 'src/app/core/services/post/post.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Post } from '../post';


@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css'],
})
export class PostDetailComponent implements OnInit {
  post: Post | undefined;
  posts: Post[] = [];

  formValue !: FormGroup;
  postModelObj: Post = new Post;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private postService: PostService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getHero();
    this.formValue = this.formBuilder.group({
      name: [''],
      description:['']
    })
  }

  getHero(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.postService.getPost(id).subscribe((post) => (this.post = post));
  }

  save(): void {
    const id =Number(this.route.snapshot.paramMap.get('id')); //?
    console.log(this.post?.id)
    if (this.post) {
      this.postService.updatePost(this.post)

        .subscribe(() => this.goBack());
    }
  }

  goBack(): void {
    this.location.back();
  }
}
