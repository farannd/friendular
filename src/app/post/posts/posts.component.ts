import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PostService } from 'src/app/core/services/post/post.service';
import { Post } from '../post';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css'],
})
export class PostsComponent implements OnInit {
  posts: Post[] = [];
  popular: Post[] = [];
  spinner: boolean = true;
  notPosts: boolean = false;

  formValue !: FormGroup;
  postModelObj: Post = new Post;

  constructor(private formBuilder: FormBuilder, private postService: PostService) {}

  ngOnInit(): void {
    this.getPosts();
    this.getRandomPosts();
    this.formValue = this.formBuilder.group({
      name: [''],
      description:['']
    })
  }

  getPosts(): void {
    this.spinner = true;
    this.postService.getPosts().subscribe((posts) => {
      this.posts = posts.reverse();
      this.spinner = false;
    });
  }

  getRandomPosts(): void {
    this.postService.getPosts().subscribe((posts) => {
      let postArr: Post[] = [];
      let iteration = 1;

      if (posts.length >= 4) iteration = 4;

      for (let i = 0; i < iteration; i++) {
        postArr.push(posts[Math.floor(Math.random() * posts.length)]);
      }
      this.popular = postArr;
    });
  }

  AddPost(){
    this.postModelObj.name = this.formValue.value.name;
    this.postModelObj.description = this.formValue.value.description;

    this.spinner = true;

    this.postService.addPost(this.postModelObj).subscribe({
      next: (v) => {console.log(v)},
      error: (e) => {
        alert("Error")
        console.log(e)},
      complete: () => {
        console.log('complete')
        alert('Data Saved')
        this.formValue.reset();
        this.getPosts();
        this.spinner = false;
      }
    })
    if (this.posts.length < 4) this.getRandomPosts();
  }

  //method DELETE POST
  DeletePost(post: Post): void {
    console.log(post)
    this.posts = this.posts.filter((p) => p !== post);
    console.log("tes: " + this.posts);
    console.log(post.id)
    this.postService.deletePost(post.id).subscribe();
    if (this.posts.length <= 4) this.getRandomPosts();
    if (this.posts.length === 0) this.notPosts = true;
  }
}
