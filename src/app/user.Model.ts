class UserRegister {
  id: number= 0;
  name: string="";
  email: string="";
  password: string="";
  phone: string="";
}

class UserLogin {
  id: number= 0;
  email: string="";
  password: string="";
}

export {UserRegister, UserLogin}
